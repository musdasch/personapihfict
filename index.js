const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const selfsigned = require('selfsigned');

const app = express();
const port = 3000;

const attrs = [{ name: 'commonName', value: 'contoso.com' }];
const pems = selfsigned.generate(attrs, { days: 365 });

const auth = function (req, res, next) {
	const authorization = req.headers.authorization;
	
	if(authorization !== undefined && authorization.startsWith("Bearer ")){
		
		token = authorization.substring(7, authorization.length);

		jwt.verify(token, pems.public, function(err, decoded) {
			if(!err){
				req.token = decoded;
				next();
			} else {
				return res.status(403).json({message: err.message});
			}
		});
	} else {
		return res.status(403).json({message: "No bearer token defined"});
	}
};

app.use(express.json())
app.use(cors());
app.use(bodyParser.json());


let persons = [];

app.get('/', [auth], (req, res) => {
	res.json(persons);
});

app.get('/:id', [auth], (req, res) => {
	if(persons[req.params.id] === undefined){
		res.json([]);
	} else {
		res.json(persons[req.params.id]);
	}
});

app.post('/', [auth], (req, res) => {
	const person = req.body;
	persons.push(person);
	res.json(person);
});


app.post('/auth', (req, res) => {
	const auth = req.body;


	if(auth.email != "test@test.ch" || auth.password != "1234")
		return res.status(401).json({message: "Wrong authentication."});

	const token = {
		token: jwt.sign({ email: auth.email }, pems.private, { algorithm: 'RS256'})
	};

	res.json(token);
});

app.put('/:id', [auth], (req, res) => {
	const person = req.body;
	persons[req.params.id] = person;
	res.json(person);
});

app.patch('/:id', [auth], (req, res) => {
	const person = req.body;

	for (var prop in person) {
		persons[req.params.id][prop] = person[prop];
	}

	res.json(persons[req.params.id]);
});

app.delete('/:id', [auth], (req, res) => {
	const person = {...persons[req.params.id]};
	delete persons[req.params.id];
	res.json(person);
});

app.listen(port, () => console.log(`PersonAPI app listening on port ${port}!`))